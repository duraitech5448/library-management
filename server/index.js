const express = require("express");
const app = express();
const mongoose = require("mongoose");
const rBooks = require("./routes/rBooks");
const bodyParser = require('body-parser');
const cors = require('cors');


/* Data base conection functionalites start */
const dbURI = 'mongodb://127.0.0.1:27017/booksDB';
mongoose.connect(dbURI, { useNewUrlParser: true, useUnifiedTopology: true });
const db = mongoose.connection;
db.on('connected', () => {
  console.log(`Connected to the database at ${dbURI}`);
});
db.on('error', (err) => {
  console.error(`Database connection error: ${err}`);
});
db.on('disconnected', () => {
  console.log('Database disconnected');
});
process.on('SIGINT', () => {
  db.close(() => {
    console.log('Database connection terminated');
    process.exit(0);
  });
});
/* Data base conection functionalites End */

/* API request handlings */
app.use(express.json());

app.use(bodyParser.json());
app.use(cors());



app.get("/",(req,res)=>{
res.json("hi Mearn stack");
})

// courses requests
app.post("/save-books",rBooks.saveBooks);
app.post("/fetch-books",rBooks.fetchBooks);
app.post("/issue-books",rBooks.issueBooks);


app.listen(5000,()=>{
    console.log("is running port 5000")
})