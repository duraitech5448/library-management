const mongoose = require('mongoose');

const issueBooksSchema = new mongoose.Schema({
    name: {
      type: String,
    },
    author: {
      type: String,
    },
    type: {
      type: String,
    },
    stuId: {
      type: String,
    },
    stuName: {
      type: String,
    },
    issueDate: {
      type: Date,
      default: new Date
    }
   
  });

module.exports = mongoose.model('issuebook', issueBooksSchema);