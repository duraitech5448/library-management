const mongoose = require('mongoose');

const booksSchema = new mongoose.Schema({
    name: {
      type: String,
    },
    author: {
      type: String,
    },
    type: {
      type: String,
    },
    status: {
      type: String,
    },
    issueDate: {
      type: Date,
      default: new Date
    },
    stuId: {
      type: String,
    },
    stuName: {
      type: String,
    },
    
   
  });

module.exports = mongoose.model('books', booksSchema);
