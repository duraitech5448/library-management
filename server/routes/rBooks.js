const express = require('express');
const mBooks = require("../models/mBooks");
const mIssueBook = require("../models/mIssueBooks");

exports.saveBooks = function(req,res){
    console.log("is coming");
    const oInputData = req.body;
    const newBooks = new mBooks({
        name : oInputData.name,
        author : oInputData.author,
        type : oInputData.bookType,
        status: "A"
    });
    newBooks.save().then(savedBooks => {
        console.log("Course saved successfully");
        res.status(201).json({ code: "SAVED_SUCCESS" });
    }).catch(err => {
        console.log("Error saving the Books", err);
        res.status(500).json({ error: 'Unknown Error saving the course' });
    });
};

exports.issueBooks = function(req,res){
    console.log("is coming");
    const oInputData = req.body;
    mBooks.updateMany({ _id: oInputData._id }, {
        status : "I",
        stuId : oInputData.stuId,
        stuName : oInputData.stuName
    })
    .then(result => {
        res.status(200).json({ code: "UPDATED_SUCCESS" });
    })
    .catch(err => {
        console.log(err);
        res.status(500).json({ error: 'Unknown Error updating the book' });
    });
};

exports.updateCourse = function (req,res){
    const oUpdtData = {
        code : oInputData.code,
        name : oInputData.name,
        description : oInputData.description,
        actulFee : oInputData.actulFee,
        discFee : oInputData.discFee,
        duration : oInputData.duration,
        regCount : oInputData.regCount,
        offers : oInputData.offers
    }
    mCourse.updateOne({ _id: req.body._id }, oUpdtData,(err, result) => {
        if (err) {
            return res.status(500).json({ error: 'Error updating the course' });
        }else{
            res.json({ message: 'UPDT_SUCCESS' });
        }
    });
};

exports.fetchBooks = async function(req,res){
    let oQury = {};
    if( req.body.type){
        oQury.type = req.body.type;
    }
    if(req.body.status){
        oQury.status = req.body.status;
    }
    try {
        const docs = await mBooks.find(oQury).exec();
        if (docs) {
            res.status(200).json({ books: docs });
        }
    } catch (err) {
        res.status(500).json({ error: 'Error finding books' });
    }
}