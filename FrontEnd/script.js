
// app.js
var app = angular.module('libraryApp', []);

app.controller('LibraryController', function($scope ,$http) {
    // $scope.books = [];
	$scope.viewBookType = undefined;
    $scope.curTab = "home";
    $scope.filterText = '';
    $scope.oIssueBook = {};

    // Define an array of book types
    $scope.bookTypes = ["JEE", "NEET", "Sixth Standard", "Seventh Standard", "Eighth Standard", "Ninth Standard", "Tenth Standard", "Eleventh Standard", "Twelfth Standard"];

    $scope.addBook = function() {
        var book = {
            name: $scope.bookName,
            author: $scope.author,
            bookType: $scope.bookType
        };
        console.log("jjjjjjjjjlllllllllllll");
        console.log(book)
         // Send a POST request to the API endpoint
         $http.post('http://localhost:5000/save-books', book)
         .then(function(response) {
			alert("Book Added Successfully");
            $scope.bookName = "";
            $scope.author = "";
            $scope.bookType = "";
         })
         .catch(function(error) {
			alert("Book can't Added ");
		})

        // Clear the form fields
        $scope.bookName = '';
        $scope.author = '';
        $scope.bookType = '';
    };

    $scope.viewBook = function(status) {
         $http.post('http://localhost:5000/fetch-books',{ status : status ,type : $scope.viewBookType})
         .then(function(response) {
			 $scope.books = response.data.books;
			 if(response.data.books && response.data.books.length == 0){
				alert("No Books Found this type");
			 }
         })
         .catch(function(error) {
			alert("Unknown Error");
         });
       
    };
    $scope.resetListBook = function(){
        $scope.books = [];
    }
    $scope.changeTab = function(type){
        $scope.books = [];
        $scope.curTab = type;
        if( type == "allbooks"){
            $http.post('http://localhost:5000/fetch-books')
         .then(function(response) {
			 $scope.books = response.data.books;
             console.log($scope.books)
			 if(response.data.books && response.data.books.length == 0){
				alert("No Books Found this type");
			 }
         })
         .catch(function(error) {
			alert("Unknown Error");
         });
        }
    }

    $scope.issueBook = function (){
        console.log("pppppppppppppppppppp")
        console.log($scope.oIssueBook);
        $http.post('http://localhost:5000/issue-books',$scope.oIssueBook)
         .then(function(response) {
				alert("Book issued successfully");
                $scope.oIssueBook = {};

         })
         .catch(function(error) {
			alert("Unknown Error");
         });

    }
    $scope.onIssue = function(book){
        $scope.oIssueBook = $scope.books[book];
        $scope.curTab = "issuebook";
    }
});
